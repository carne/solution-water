package water;

public class Water {
    public static void main(String[] args) {
        int[] arr = new int[]{2, 3, 1, 2, 3, 5, 2, 3};
        System.out.println(getWaterValue(arr));
    }

    public static int getWaterValue(int[] input) {
        int result = 0;
        for (int i = 0; i < input.length; i++) {
            int currentValue = input[i];
            int right = 0;
            for (int j = i; j < input.length; j++) {
                right = Math.max(input[j], right);
            }
            int left = 0;
            for (int j = i; j >= 0 ; j--) {
                left = Math.max(input[j], left);
            }
            result += Math.min(right, left) - currentValue;
        }
        return result;
    }
}